<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Rdv;
use AppBundle\Entity\User;
use AppBundle\Entity\Session AS SessionOfRdv;
use AppBundle\Entity\Participation;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


class AdController extends Controller
{

      public function rdvsOfSubCategoryAction(Request $request, $subCategoryid)
    {


      return $this->render('AppBundle:Ad:index.html.twig', array(
          'categories' => $aCategories,
          'rdvs' => $oPagination,
          'citys' => $citys,
          )
      );

    }

      public function participationAction(Request $request)
    {
      $session_id = $request->get('session');
      $user = $this->getUser();
      $random = random_int(1000000, 9999999);

      $entityManager = $this->getDoctrine()->getManager();
      $session = $entityManager->getRepository(SessionOfRdv::class)->find($session_id);
      $Rdvid = $session->getRdv();
      $Rdv = $entityManager->getRepository('AppBundle:Rdv')->find($Rdvid);

      $participationExist = $entityManager->getRepository(Participation::class)->findOneBy(
      array('user' => $user , 'session' => $session )
      );
     if($participationExist)
     {
       $this->addFlash(
            'notice',
            'Vous êtes déja participés a cette session'
        );

        return $this->redirectToRoute('ad_details',array('id' => $Rdvid->getId()));
      /* return $this->render('AppBundle:Ad:details.html.twig', array(
           'rdv' => $Rdv,
       ));*/


     }

      $participation = new Participation();
      $participation->setUser($user);
      $participation->setSession($session);
      $participation->setTicketKey($random);

      $entityManager->persist($participation);
      $entityManager->flush();


      $this->addFlash(
           'notice',
           'Vous êtes bien inscris a cette session'
       );

      return $this->redirectToRoute('ad_details',array('id' => $Rdvid->getId()));

    }


    public function indexAction(Request $poRequest,$subcateg)
    {
		$session = $session = $this->get('session');

		if(!$poRequest->get('city') and !$poRequest->get('delegation') and !$poRequest->get('order')){
		$session->set('selectedCity', NULL);
		$session->set('selectedDelegation', NULL);
		$session->set('order', NULL);
		}


        $order = false;
		$cityDql ='SELECT r
					FROM AppBundle:Rdv r
					INNER JOIN AppBundle:Session s WITH s.rdv = r
					INNER JOIN AppBundle:Place p WITH p = s.place
					INNER JOIN AppBundle:City c WITH c =  p.city
					WHERE  c.id = :city AND r.published = TRUE ';
		$delegationDql ='SELECT r
						FROM AppBundle:Rdv r
						INNER JOIN AppBundle:Session s with s.rdv = r
						INNER JOIN AppBundle:Place p WITH p = s.place
						INNER JOIN AppBundle:Delegation d WITH d = IDENTITY(p.delegation)
						WHERE  d.id = :delegation AND r.published = TRUE ';
		$notFiltredDql = 'SELECT r
						FROM AppBundle:Rdv r
						WHERE  r.published = TRUE';

        $oEm = $this->getDoctrine()->getManager();
        $aCategories = $oEm->getRepository('AppBundle:Category')->findAll();
        $oQueryBuilder = $oEm->getRepository('AppBundle:Rdv')
                        ->createQueryBuilder('r')
                        ->where('r.published = TRUE');
        if($subcateg != 0){
          $oQueryBuilder = $oQueryBuilder->andWhere('r.subCategory = :subcateg');
        }
		$oRdvsQuery = $oQueryBuilder->getQuery();

        if($poRequest->get('order')){
		$session->set('order', $poRequest->get('order'));
		}

    if($subcateg != 0){
      //var_dump($subcateg);die();
      $delegationDql .= ' AND r.subCategory = :subcateg ';
      $cityDql .= ' AND r.subCategory = :subcateg ';
      $notFiltredDql .= ' AND r.subCategory = :subcateg ' ;

    }

    if($session->get('order')){

        // $session->set('order', $poRequest->get('order'));
		 if($session->get('order')=='pr'){
		  $delegationDql .= ' ORDER BY r.createdAt DESC';
		  $cityDql .= ' ORDER BY r.createdAt DESC';
		  $notFiltredDql .= ' ORDER BY r.createdAt DESC';
		 }
		 if($session->get('order')=='mc'){
		  $delegationDql .= ' ORDER BY r.price ASC';
		  $cityDql .= ' ORDER BY r.price ASC';
		  $notFiltredDql .= ' ORDER BY r.price ASC';
		  //var_dump($cityDql);
		 }
		 if($session->get('order')=='pv'){
		  $delegationDql .= ' ORDER BY r.note DESC';
		  $cityDql .= ' ORDER BY r.note DESC';
		  $notFiltredDql .= ' ORDER BY r.note DESC';
		 }
		 //var_dump($this->get('session')->get('selectedCity'));
		 //echo("i am here");
		 if ( $this->get('session')->get('selectedDelegation')){

			 $oRdvsQuery = $oEm->createQuery($delegationDql)->setParameter('delegation', $this->get('session')->get('selectedDelegation'));


			 }

		 if ($this->get('session')->get('selectedCity')){
			 $oRdvsQuery = $oEm->createQuery($cityDql)->setParameter('city', $this->get('session')->get('selectedCity'));


		 }

		 if ( !$this->get('session')->get('selectedDelegation') and !$this->get('session')->get('selectedCity')){

			 $oRdvsQuery = $oEm->createQuery($notFiltredDql);
			 $order = true;
       //var_dump($oRdvsQuery);die();

		 }
        }

		if($poRequest->get('city') and $poRequest->get('city') != 'allCity' ){

			            // set and get session attributes
	                	$session->set('selectedCity', $poRequest->get('city'));
					    $session->set('selectedDelegation', NULL);

						$oRdvsQuery = $oEm->createQuery($cityDql)->setParameter('city', $poRequest->get('city'));

						//$oQueryBuilder = $oQueryBuilder->getResult();
						//var_dump($oQueryBuilder);die();
		}

		if($poRequest->get('delegation') and $poRequest->get('delegation') != 'allDelegation'){
			            $session->set('selectedDelegation', $poRequest->get('delegation'));
						$session->set('selectedCity', NULL);

						$oRdvsQuery = $oEm->createQuery($delegationDql)->setParameter('delegation', $poRequest->get('delegation'));

						//$oQueryBuilder = $oQueryBuilder->getResult();
						//var_dump($oQueryBuilder);die();
		}


    if($subcateg != 0){
    //  var_dump($oRdvsQuery);die();
      $oRdvsQuery->setParameter('subcateg', $subcateg);
    }

        $oPaginator = $this->get('knp_paginator');
        $oPagination = $oPaginator->paginate(
            $oRdvsQuery,
            $poRequest->query->getInt('page', 1),
            6
        );

		$citys = $oEm->getRepository('AppBundle:City')->findAll();
        if($poRequest->get('city') or $poRequest->get('delegation') or $this->get('session')->get('selectedCity') or $this->get('session')->get('selectedDelegation') or $order == true ){
		return $this->render('AppBundle:Ad:rdvListBloc.html.twig', array(
            'categories' => $aCategories,
                'rdvs' => $oPagination,
				'citys' => $citys,
            )
		);
	}

        return $this->render('AppBundle:Ad:index.html.twig', array(
            'categories' => $aCategories,
                'rdvs' => $oPagination,
				'citys' => $citys,
            )
        );
    }

	public function delegationAction(Request $request)
    {

		$city_id = $request->get('city');
	    $Em = $this->getDoctrine()->getManager();
        $delegations = $Em->getRepository('AppBundle:Delegation')->findBycity($city_id);
		//var_dump($delegations); die();

		  //$delegations = $delegations->getResult();
	      $arrayOfdelegations=array();

	      foreach($delegations as $d)
		  {
			$name = $d->getName();
			$id = $d->getId();

			//$arrayOfTags = array(array("id"=>$id,"name"=>$name));
			array_push($arrayOfdelegations,array("id"=>$id,"value"=>$name));

	     }

     $jsonOfdelegations = json_encode($arrayOfdelegations);
	 if (isset($jsonOfdelegations))
	 //var_dump($jsonOfTags);

		return new Response($jsonOfdelegations);
	}
	 public function autocompleteAction(Request $Request)
    {

	   $term = $Request->get('term');
	   $entityManager = $this->getDoctrine()->getManager();
	   $query = $entityManager->createQuery(
    'SELECT t
    FROM AppBundle:Tag t
    WHERE t.name LIKE :name
    '
      )->setParameter('name', '%'.$term.'%');

     $tags = $query->getResult();
	 $arrayOfTags=array();

	 foreach($tags as $t)
{
    $name = $t->getName();
    $id = $t->getRdv()->getId();
	$title = $t->getRdv()->getTitle();

    //$arrayOfTags = array(array("id"=>$id,"name"=>$name));
	array_push($arrayOfTags,array("id"=>$id,"tag"=>$name,"value"=>$title));

}

     $jsonOfTags = json_encode($arrayOfTags);
	 if (isset($jsonOfTags))
	 //var_dump($jsonOfTags);

		return new Response($jsonOfTags
        );
	}

    public function detailsAction($id)
    {
        $oEm = $this->getDoctrine()->getManager();
        $oRdv = $oEm->getRepository('AppBundle:Rdv')->find($id);


        return $this->render('AppBundle:Ad:details.html.twig', array(
            'rdv' => $oRdv,
        ));
    }

    public function newAction(Request $poRequest, \Swift_Mailer $mailer)
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUser = $this->getUser();

        //Create Ad object
        $oRdv = new Rdv();
        $oRdv->setAuthor($oUser);

        //Binding form flow
        $oFlow = $this->get('app.form.flow.rdv');
        $oFlow->bind($oRdv);

        //Current step form
        $oForm = $oFlow->createForm();

        if ($oFlow->isValid($oForm)) {
            $oFlow->saveCurrentStepData($oForm);

            if ($oFlow->nextStep()) {
                // form for the next step
                $oForm = $oFlow->createForm();
            } else {
                // flow finished
                $oEm->persist($oRdv);
                $oEm->flush();

                $oFlow->reset(); // remove step data from the session

                //Add info flash message
                $this->addFlash('success', "Cette annonce est en attente de validation de l'administrateur pour qu'elle soit visible aux autres utilisateurs.");

				$message = (new \Swift_Message('5outh-Rdv'))
        ->setFrom('5outh-rdv@5outh-rdv.com')
        ->setTo('pakristation@hotmail.fr')
        ->setBody(
            $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                'Emails/RvdWaitigForApproval.html.twig'
            ),
            'text/html'
        )
		 ;

    $mailer->send($message);

                return $this->redirect($this->generateUrl('ad_details', ['id' => $oRdv->getId()])); // redirect when done
            }
        }


        return $this->render('AppBundle:Ad:new.html.twig', [
            'flow' => $oFlow,
            'form' => $oForm->createView(),
        ]);
    }

    protected function addFlash($sType, $sMessage) {
        $flashbag = $this->get('session')->getFlashBag();

        // Add flash message
        $flashbag->add($sType, $sMessage);
    }

}
