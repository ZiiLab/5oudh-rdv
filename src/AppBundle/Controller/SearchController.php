<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class SearchController extends Controller
{
    public function searchBarAction()
    {
      $form = $this->createFormBuilder()
      ->add('search', SearchType::class, array( 'attr' => array('placeholder' => 'Recherche') ))
            ->add('send', SubmitType::class, array('label' => 'Chercher', 'attr' => array('class' => 'btn btn-outline-warning my-2 my-sm-0')))
            ->getForm();
        return $this->render('AppBundle:search:searchbar.html.twig', array(
          'form' => $form->createView(),
        ));
    }

    public function searchHandleAction(Request $poRequest)
    {
    //  var_dump($request->request);die();

      //var_dump($data['search']); die();
      $session = $session = $this->get('session');
      if( $poRequest->request->get('form')){
      $data = $poRequest->request->get('form');
      $session->set('searchTerm', $data['search']);
      }
      $searchTerm = $session->get('searchTerm');
      if(!$poRequest->get('city') and !$poRequest->get('delegation') and !$poRequest->get('order')){
      $session->set('selectedCity', NULL);
      $session->set('selectedDelegation', NULL);
      $session->set('order', NULL);
      }


          $order = false;
      $cityDql ='SELECT r
            FROM AppBundle:Rdv r
            INNER JOIN AppBundle:Tag t WITH t.rdv = r AND t.name = :searchTerm
            INNER JOIN AppBundle:Session s WITH s.rdv = r
            INNER JOIN AppBundle:Place p WITH p = s.place
            INNER JOIN AppBundle:City c WITH c =  p.city
            WHERE  c.id = :city AND r.published = TRUE';
      $delegationDql ='SELECT r
              FROM AppBundle:Rdv r
              INNER JOIN AppBundle:Tag t WITH t.rdv = r AND t.name = :searchTerm
              INNER JOIN AppBundle:Session s with s.rdv = r
              INNER JOIN AppBundle:Place p WITH p = s.place
              INNER JOIN AppBundle:Delegation d WITH d = IDENTITY(p.delegation)
              WHERE  d.id = :delegation AND r.published = TRUE ';
      $notFiltredDql = 'SELECT r
              FROM AppBundle:Rdv r
              INNER JOIN AppBundle:Tag t WITH t.rdv = r AND t.name = :searchTerm
              WHERE  r.published = TRUE';

          $oEm = $this->getDoctrine()->getManager();
          $aCategories = $oEm->getRepository('AppBundle:Category')->findAll();
          $oQueryBuilder = $oEm->createQuery($notFiltredDql);

      /*    if($subcateg != 0){
            $oQueryBuilder = $oQueryBuilder->andWhere('r.subCategory = :subcateg');
          }*/
      $oRdvsQuery = $oQueryBuilder ; //->getQuery()

          if($poRequest->get('order')){
      $session->set('order', $poRequest->get('order'));
      }

    /*  if($subcateg != 0){
        //var_dump($subcateg);die();
        $delegationDql .= ' AND r.subCategory = :subcateg ';
        $cityDql .= ' AND r.subCategory = :subcateg ';
        $notFiltredDql .= ' AND r.subCategory = :subcateg ' ;

      }*/

      if($session->get('order')){

          // $session->set('order', $poRequest->get('order'));
       if($session->get('order')=='pr'){
        $delegationDql .= ' ORDER BY r.createdAt DESC';
        $cityDql .= ' ORDER BY r.createdAt DESC';
        $notFiltredDql .= ' ORDER BY r.createdAt DESC';
       }
       if($session->get('order')=='mc'){
        $delegationDql .= ' ORDER BY r.price ASC';
        $cityDql .= ' ORDER BY r.price ASC';
        $notFiltredDql .= ' ORDER BY r.price ASC';
        //var_dump($cityDql);
       }
       if($session->get('order')=='pv'){
        $delegationDql .= ' ORDER BY r.note DESC';
        $cityDql .= ' ORDER BY r.note DESC';
        $notFiltredDql .= ' ORDER BY r.note DESC';
       }
       //var_dump($this->get('session')->get('selectedCity'));
       //echo("i am here");

       $delegationDql .= ' GROUP BY r.id';
       $cityDql .= ' GROUP BY r.id';
       $notFiltredDql .= ' GROUP BY r.id';

       if ( $this->get('session')->get('selectedDelegation')){

         $oRdvsQuery = $oEm->createQuery($delegationDql)->setParameter('delegation', $this->get('session')->get('selectedDelegation'));


         }

       if ($this->get('session')->get('selectedCity')){
         $oRdvsQuery = $oEm->createQuery($cityDql)->setParameter('city', $this->get('session')->get('selectedCity'));


       }

       if ( !$this->get('session')->get('selectedDelegation') and !$this->get('session')->get('selectedCity')){

         $oRdvsQuery = $oEm->createQuery($notFiltredDql);
         $order = true;
         //var_dump($oRdvsQuery);die();

       }
          }

      if($poRequest->get('city') and $poRequest->get('city') != 'allCity' ){

                    // set and get session attributes
                      $session->set('selectedCity', $poRequest->get('city'));
                $session->set('selectedDelegation', NULL);

              $oRdvsQuery = $oEm->createQuery($cityDql)->setParameter('city', $poRequest->get('city'));

              //$oQueryBuilder = $oQueryBuilder->getResult();
              //var_dump($oQueryBuilder);die();
      }

      if($poRequest->get('delegation') and $poRequest->get('delegation') != 'allDelegation'){
                    $session->set('selectedDelegation', $poRequest->get('delegation'));
              $session->set('selectedCity', NULL);

              $oRdvsQuery = $oEm->createQuery($delegationDql)->setParameter('delegation', $poRequest->get('delegation'));

              //$oQueryBuilder = $oQueryBuilder->getResult();
              //var_dump($oQueryBuilder);die();
      }


  /*    if($subcateg != 0){
      //  var_dump($oRdvsQuery);die();
        $oRdvsQuery->setParameter('subcateg', $subcateg);
      }*/
          $oRdvsQuery = $oRdvsQuery->setParameter('searchTerm',$searchTerm);
          $oPaginator = $this->get('knp_paginator');
          $oPagination = $oPaginator->paginate(
              $oRdvsQuery,
              $poRequest->query->getInt('page', 1),
              12
          );

      $citys = $oEm->getRepository('AppBundle:City')->findAll();
          if($poRequest->get('city') or $poRequest->get('delegation') or $this->get('session')->get('selectedCity') or $this->get('session')->get('selectedDelegation') or $order == true ){
      return $this->render('AppBundle:Ad:rdvListBloc.html.twig', array(
              'categories' => $aCategories,
                  'rdvs' => $oPagination,
          'citys' => $citys,
              )
      );
      }

          return $this->render('AppBundle:search:searchresult.html.twig', array(
              'categories' => $aCategories,
                  'rdvs' => $oPagination,
          'citys' => $citys,
              )
          );

///////////////////////////////////////////////////////////////////

    }

}
