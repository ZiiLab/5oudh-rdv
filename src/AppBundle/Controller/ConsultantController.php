<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ConsultantController extends Controller
{
    public function profileAction($id)
    {
        $oEm = $this->getDoctrine()->getManager();
        $oConsultant = $oEm->getRepository('AppBundle:User')->find($id);

        return $this->render('AppBundle:consultant:profile.html.twig', array(
           'consultant' => $oConsultant,
        ));
    }
}