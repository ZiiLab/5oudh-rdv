<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityController extends BaseController
{
    public function renderLogin(array $aData)
    {
        $oRequest = $this->container->get('request_stack')->getCurrentRequest();
        if ($oRequest->get('_route') == 'admin_login') {
            $sTemplate = sprintf('AppBundle:admin:login.html.twig');
        }
        else {
            $sTemplate = sprintf('FOSUserBundle:Security:login.html.twig');
        }
        return $this->container->get('templating')->renderResponse($sTemplate, $aData);
    }
}