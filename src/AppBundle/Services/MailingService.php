<?php

/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */
namespace AppBundle\Services;

class MailingService
{
    private $sSender;
    private $oTemplating;
    private $oMailer;

    /**
     * MailingService constructor.
     *
     * @param $psSender
     * @param \Swift_Mailer $poMailer
     * @param \Twig_Environment $poTemplating
     */
    public function __construct($psSender, \Swift_Mailer $poMailer, \Twig_Environment $poTemplating)
    {
        $this->sSender = $psSender;
        $this->oMailer = $poMailer;
        $this->oTemplating = $poTemplating;
    }

    /**
     * This method is used to prepare the email to be sent
     *
     * @param $psSubject
     * @param $paRecipient
     * @param $paData
     * @param $psTemplate
     * @param null $paImages
     * @return $this
     */
    public function makeEmail($psSubject, $paRecipient, $paData, $psTemplate, $paImages = null)
    {
        $oEmail = (new \Swift_Message())
            ->setSubject($psSubject)
            ->setFrom([$this->sSender => '5oudh-RDV'])
            ->setTo($paRecipient);

        if($paImages)
            foreach ($paImages as $sKey => $sValue)
                $paImages[$sKey] = $oEmail->embed(\Swift_Image::fromPath($sValue));

        $oEmail->setBody(
            $this->oTemplating->render(
                $psTemplate,
                $paImages ? array_merge($paData, $paImages) : $paData
            ),
            'text/html'
        );

        return $oEmail;
    }

    /**
     * This method is used to send the email
     *
     * @param $poEmail
     */
    public function sendEmail($poEmail)
    {
        $this->oMailer->send($poEmail);
    }
}