<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * Rdv
 * @Vich\Uploadable
 */
class Rdv
{
    use TimestampableTrait;
    #region *******************Attributes region***************************

    /**
     * @var int
     */
    private $id;
	
	 /**
     * @var int
     */
    private $placeId;
	
	/**
     * @var decimal
     */
    private $note;
	
	/**
     * @var decimal
     */
    private $price;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $niveau;

    /**
     * @var File
     * @Vich\UploadableField(mapping="ad_image", fileNameProperty="imageName")
     */
    private $imageFile;

    /**
     * @var string
     */
    private $imageName;

    /**
     * @var boolean
     */
    private $published;

    #endregion

    #region *******************Mappings region***************************

    /**
     * @var ArrayCollection
     */
    private $tags;

    /**
     * @var Category
     */
    private $category;

    /**
     * @var SubCategory
     */
    private $subCategory;

    /**
     * @var ArrayCollection
     */
    private $sessions;

    /**
     * @var User
     */
    private $author;
	
	/**
     * @var Place
     */
    private $place;
	
	/**
     * @var Partitipation
     */
    private $participation;

    #endregion

    #region *******************Methods region***************************

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
	


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Rdv
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
	
	 /**
     * Get price
     *
     * @return decimal
     */
    public function getPrice()
    {
        return $this->price;
    }
	
	/**
     * Get note
     *
     * @return decimal
     */
    public function getNote()
    {
        return $this->note;
    }
	
	 /**
	 * Get place
	 *
     * @return place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param int $place
	 *
     * @return Rdv
     */
    public function setPlace($place)
    {
        $this->place = $place;
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Rdv
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * @param string $niveau
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     */
    public function addTag($tag)
    {
        $tag->setRdv($this);
        $this->tags->add($tag);
        return $this;
    }

    /**
     * @param Tag $tag
     */
    public function removeTag($tag)
    {
        $this->tags->removeElement($tag);
        return $this;
    }

    /**
     * @param ArrayCollection $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return SubCategory
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * @param SubCategory $subCategory
     */
    public function setSubCategory($subCategory)
    {
        $this->subCategory = $subCategory;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * @var ArrayCollection
     */
    public function getRemainingSessions()
    {
        $oTodayDate = new \DateTime('NOW');

        return $this->sessions->filter(function (Session $oSession) use ($oTodayDate){
            if ($oSession->getDateSession() > $oTodayDate)
                return $oSession;
        });
    }

    /**
     * @param ArrayCollection $sessions
     */
    public function setSessions($sessions)
    {
        $this->sessions = $sessions;
        return $this;
    }

    /**
     * @param Session $session
     */
    public function addSession($session)
    {
        $session->setRdv($this);
        $this->sessions->add($session);
        return $this;
    }

    /**
     * @param Session $session
     */
    public function removeSession($session)
    {
        $this->sessions->removeElement($session);
        return $this;
    }

    /**
     * @return Session
     */
    public function getClosestSession() {
        $oTodayDate = new \DateTime('NOW');

        $cValidSessions = $this->sessions->filter(function (Session $oSession) use ($oTodayDate){
            if ($oSession->getDateSession() > $oTodayDate)
                return $oSession;
        });

        $oIterator = $cValidSessions->getIterator();
        $oIterator->uasort(function (Session $oSession1, Session $oSession2){
            return ($oSession1->getDateSession() > $oSession2->getDateSession()) ?1 : -1;
        });

        return (new ArrayCollection(iterator_to_array($oIterator)))->first();
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }
	
	

    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->published;
    }

    /**
     * @param bool $published
     */
    public function setPublished(bool $published)
    {
        $this->published = $published;
        return $this;
    }



    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function validate(ExecutionContextInterface $context)
    {

        //Wrong file MIME type
        if ($this->imageFile && !in_array($this->imageFile->getMimeType(), array(
                'video/bmp',
                'image/jpeg',
                'image/png'
            ))) {
            $context
                ->buildViolation("Format d'image non approprié")
                ->atPath('imageName')
                ->addViolation()
            ;

        }
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->published = false;
    }

    #endregion
}

