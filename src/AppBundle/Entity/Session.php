<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * Session
 */
class Session
{
    use TimestampableTrait;
    #region *******************Attributes region***************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateSession;

    /**
     * @var \DateTime
     */
    private $startTime;

    /**
     * @var \DateTime
     */
    private $endTime;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $nbrParticipants;

    #endregion

    #region *******************Mappings region***************************

    /**
     * @var Place
     */
    private $place;

    /**
     * @var Rdv
     */
    private $rdv;

    #endregion

    #region *******************Methods region***************************
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Id $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * Set dateSession
     *
     * @param \DateTime $dateSession
     *
     * @return Session
     */
    public function setDateSession($dateSession)
    {
        $this->dateSession = $dateSession;

        return $this;
    }

    /**
     * Get dateSession
     *
     * @return \DateTime
     */
    public function getDateSession()
    {
        return $this->dateSession;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Session
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Session
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Session
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set nbrParticipants
     *
     * @param integer $nbrParticipants
     *
     * @return Session
     */
    public function setNbrParticipants($nbrParticipants)
    {
        $this->nbrParticipants = $nbrParticipants;

        return $this;
    }

    /**
     * Get nbrParticipants
     *
     * @return int
     */
    public function getNbrParticipants()
    {
        return $this->nbrParticipants;
    }

    /**
     * @return Place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param Place $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
        return $this;
    }

    /**
     * @return Rdv
     */
    public function getRdv()
    {
        return $this->rdv;
    }

    /**
     * @param Rdv $rdv
     */
    public function setRdv($rdv)
    {
        $this->rdv = $rdv;
        return $this;
    }

    public function __toString()
    {
        return sprintf("%s %s %s-%s-%s",
            $this->getDateSession()->format('d/M/Y'),
            $this->startTime->format('H:i'),
            $this->place->getAddress(),
            $this->place->getDelegation(),
            $this->place->getCity());
    }


    #endregion
}
