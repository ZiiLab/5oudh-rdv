<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use AppBundle\Entity\Traits\TimestampableTrait;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * User
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    use TimestampableTrait;

    #region **************************Attributes Region**************************************

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    private $cin;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var date
     */
    private $birthDate;
    /**
     * @var int
     */
    private $phone;

    /**
     * @var int
     */
    private $zip;

    /**
     * @var string
     */
    private $biography;

    /**
     * @var string
     */
    private $education;

    /**
     * @var string
     */
    private $proSituation;

    /**
     * @var string
     */
    private $address;

    /**
     * @var float
     */
    private $balance;

    /**
     * @var int
     */
    private $nbrViews;

    /**
     * @var float
     */
    private $profileCompletionPercent;

    /**
     * @var boolean
     */
    private $admin;

    /**
     * @var File
     * @Vich\UploadableField(mapping="profile_image", fileNameProperty="imageName")
     */
    private $imageFile;

    /**
     * @var string
     */
    private $imageName;

    #endregion

    #region **************************Mappings Region*************************************

    /**
     * @var City
     */
    private $city;

    /**
     * @var Delegation
     */
    private $delegation;

    /**
     * @var ArrayCollection
     */
    private $rdvs;

    /**
     * @var ArrayCollection
     */
    private $diplomas;

    /**
     * @var ArrayCollection
     */
    private $professionalExperiences;

    /**
     * @var ArrayCollection
     */
    private $interests;

	/**
     * @var Partitipation
     */
    private $participation;

    #endregion

    #region **************************Methods Region*************************************

    /**
     * @return string
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * @param string $cin
     */
    public function setCin($cin)
    {
        $this->cin = $cin;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return string
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * @param string $biography
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;
        return $this;
    }

    /**
     * @return string
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * @param string $education
     */
    public function setEducation($education)
    {
        $this->education = $education;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbrViews()
    {
        return $this->nbrViews;
    }

    /**
     * @param int $nbrViews
     */
    public function setNbrViews($nbrViews)
    {
        $this->nbrViews = $nbrViews;
        return $this;
    }

    /**
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param int $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return int
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param int $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getProSituation()
    {
        return $this->proSituation;
    }

    /**
     * @param string $proSituation
     */
    public function setProSituation($proSituation)
    {
        $this->proSituation = $proSituation;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRdvs()
    {
        return $this->rdvs;
    }

    /**
     * @param ArrayCollection $rdvs
     */
    public function setRdvs($rdvs)
    {
        $this->rdvs = $rdvs;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * @param bool $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
        return $this;
    }

    /**
     * @return date
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param date $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return Delegation
     */
    public function getDelegation()
    {
        return $this->delegation;
    }

    /**
     * @param Delegation $delegation
     */
    public function setDelegation($delegation)
    {
        $this->delegation = $delegation;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDiplomas()
    {
        return $this->diplomas;
    }

    /**
     * @param ArrayCollection $diplomas
     */
    public function setDiplomas($diplomas)
    {
        $this->diplomas = $diplomas;
        return $this;
    }

    /**
     * @var Education $diploma
     */
    public function addDiploma(Education $diploma)
    {
        $diploma->setUser($this);
        $this->diplomas->add($diploma);
    }

    /**
     * @var Education $diploma
     */
    public function removeDiploma(Education $diploma)
    {
        $this->diplomas->removeElement($diploma);
    }

    /**
     * @return ArrayCollection
     */
    public function getProfessionalExperiences()
    {
        return $this->professionalExperiences;
    }

    /**
     * @param ArrayCollection $professionalExperiences
     */
    public function setProfessionalExperiences($professionalExperiences)
    {
        $this->professionalExperiences = $professionalExperiences;
        return $this;
    }

    /**
     * @var ProfessionalExperience $experience
     */
    public function addProfessionalExperience($experience)
    {
        $experience->setUser($this);
        $this->professionalExperiences->add($experience);
    }

    /**
     * @var ProfessionalExperience $experience
     */
    public function removeProfessionalExperience($experience)
    {
        $this->professionalExperiences->removeElement($experience);
    }

    /**
     * @return float
     */
    public function getProfileCompletionPercent()
    {
      $sum=0;
      if(!is_null($this->city))
      $sum++;
      if(!is_null($this->delegation))
      $sum++;
      if(!is_null($this->cin))
      $sum++;
      if(!is_null($this->firstName))
      $sum++;
      if(!is_null($this->lastName))
      $sum++;
      if(!is_null($this->gender))
      $sum++;
      if(!is_null($this->birthDate))
      $sum++;
      if(!is_null($this->phone))
      $sum++;
      if(!is_null($this->zip))
      $sum++;
      if(!is_null($this->biography))
      $sum++;
      if(!is_null($this->education))
      $sum++;
      if(!is_null($this->address))
      $sum++;
      if(!is_null($this->proSituation))
      $sum++;
      if(!is_null($this->imageName))
      $sum++;

      return round(($sum/14)*100,2);


    }

    /**
     * @param float $profileCompletionPercent
     */
    public function setProfileCompletionPercent($profileCompletionPercent)
    {
        $this->profileCompletionPercent = $profileCompletionPercent;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getInterests()
    {
        return $this->interests;
    }

    /**
     * @param ArrayCollection $interests
     */
    public function setInterests($interests)
    {
        $this->interests = $interests;
        return $this;
    }

    /**
     * @var Interest $interest
     */
    public function addInterest(Interest $interest)
    {
        $this->interests->add($interest);
    }

    public function removeInterest(Interest $interest)
    {
        $this->interests->removeElement($interest);
    }

    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;

        if (null !== $image) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function validate(ExecutionContextInterface $context)
    {

        //Wrong file MIME type
        if ($this->imageFile && !in_array($this->imageFile->getMimeType(), array(
            'video/bmp',
            'image/jpeg',
            'image/png'
        ))) {
            $context
                ->buildViolation("Format d'image non approprié")
                ->atPath('imageName')
                ->addViolation()
            ;

        }
    }

    public function setAttributes()
    {
        $this->admin = false;
    }

    public function __construct()
    {
      parent::__construct();

      $this->balance = 0;
      $this->profileCompletionPercent = 0;
      $this->interests = new ArrayCollection();
      $this->professionalExperiences = new ArrayCollection();
      $this->diplomas = new ArrayCollection();
    }

    #endregion
}
