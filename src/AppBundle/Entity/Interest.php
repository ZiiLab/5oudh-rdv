<?php

namespace AppBundle\Entity;

/**
 * Interest
 */
class Interest
{

    #region **************************Attributes Region**************************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;
    #endregion

    #region **************************Methods Region**************************************

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Interest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }
    #endregion
}
