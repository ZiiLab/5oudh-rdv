<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use AppBundle\Tools;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 */
class Category
{
    use TimestampableTrait;

    #region **************************Attributes Region**************************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    #endregion

    #region **************************Mappings Region**************************************

    /**
     * @var ArrayCollection
     */
    private $subCategories;

    /**
     * @var ArrayCollection
     */
    private $rdvs;

    #endregion

    #region **************************Methods Region**************************************
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getSubCategories()
    {
        return $this->subCategories;
    }

    /**
     * @param SubCategory $subCategory
     */
    public function addSubCategory($subCategory)
    {
        $subCategory->setCategory($this);
        $this->subCategories[] = $subCategory;
        return $this;
    }

    /**
     * @param ArrayCollection $subCategories
     */
    public function setSubCategories($subCategories)
    {
        $this->subCategories = $subCategories;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRdvs()
    {
        return $this->rdvs;
    }

    /**
     * @param ArrayCollection $rdvs
     */
    public function setRdvs($rdvs)
    {
        $this->rdvs = $rdvs;
        return $this;
    }


    public function setAttributes()
    {
        $this->slug = Tools::slugify($this->name);
    }

    public function __construct()
    {
        $this->subCategories = new ArrayCollection();
        $this->rdvs = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    #endregion
}

