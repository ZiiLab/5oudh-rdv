<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use AppBundle\Tools;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Vote
 */
class Vote
{
	
	
    use TimestampableTrait;
    #region *******************Attributes region***************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $value;


    #endregion

    #region *******************Mappings region***************************

      /**
     * @ORM\ManyToOne(targetEntity="Rdv", inversedBy="votes")
     * @ORM\JoinColumn(name="rdv_id", referencedColumnName="id")
     */
    private $rdv;
	
	/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="votes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    #endregion

    #region *******************Methods region***************************

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Tag
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * @return Rdv
     */
    public function getRdv()
    {
        return $this->rdv;
    }
	
	/**
     * @param Rdv $rdv
     */
    public function setRdv($rdv)
    {
        $this->rdv = $rdv;
        return $this;
    }


    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }
	
	    /**
     * @return Usser
     */
    public function getUser()
    {
        return $this->user;
    }

    
    /**
     * toString
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }


    #endregion
}

