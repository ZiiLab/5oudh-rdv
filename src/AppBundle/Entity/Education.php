<?php

namespace AppBundle\Entity;

/**
 * Education
 */
class Education
{
    #region **************************Attributes Region**************************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $school;

    /**
     * @var string
     */
    private $diploma;

    /**
     * @var string
     */
    private $studyField;

    /**
     * @var \DateTime
     */
    private $startYear;

    /**
     * @var \DateTime
     */
    private $endYear;

    /**
     * @var string
     */
    private $description;
    #endregion

    #region **************************Mappings Region**************************************
    /**
     * @var User
     */
    private $user;
    #endregion

    #region **************************Methods Region**************************************
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set school
     *
     * @param string $school
     *
     * @return Education
     */
    public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return string
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set diploma
     *
     * @param string $diploma
     *
     * @return Education
     */
    public function setDiploma($diploma)
    {
        $this->diploma = $diploma;

        return $this;
    }

    /**
     * Get diploma
     *
     * @return string
     */
    public function getDiploma()
    {
        return $this->diploma;
    }

    /**
     * Set studyField
     *
     * @param string $studyField
     *
     * @return Education
     */
    public function setStudyField($studyField)
    {
        $this->studyField = $studyField;

        return $this;
    }

    /**
     * Get studyField
     *
     * @return string
     */
    public function getStudyField()
    {
        return $this->studyField;
    }

    /**
     * Set startYear
     *
     * @param \DateTime $startYear
     *
     * @return Education
     */
    public function setStartYear($startYear)
    {
        $this->startYear = $startYear;

        return $this;
    }

    /**
     * Get startYear
     *
     * @return \DateTime
     */
    public function getStartYear()
    {
        return $this->startYear;
    }

    /**
     * Set endYear
     *
     * @param \DateTime $endYear
     *
     * @return Education
     */
    public function setEndYear($endYear)
    {
        $this->endYear = $endYear;

        return $this;
    }

    /**
     * Get endYear
     *
     * @return \DateTime
     */
    public function getEndYear()
    {
        return $this->endYear;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Education
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

//    public function __toString()
//    {
//        $sStartYear = $this->startYear->format('Y');
//        $sEndYear = $this->endYear->format('Y');
//        return "$this->diploma - $this->school - $this->studyField - $sStartYear - $sEndYear";
//    }


    #endregion
}

