<?php

namespace AppBundle\Entity;

/**
 * ProfessionalExperience
 */
class ProfessionalExperience
{
    #region **************************Attributes Region**************************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $entreprise;

    /**
     * @var string
     */
    private $place;

    /**
     * @var \DateTime
     */
    private $startYear;

    /**
     * @var \DateTime
     */
    private $endYear;

    /**
     * @var string
     */
    private $description;
    #endregion

    #region **************************Mappings Region**************************************
    /**
     * @var User
     */
    private $user;
    #endregion

    #region **************************Methods Region**************************************
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ProfessionalExperience
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set entreprise
     *
     * @param string $entreprise
     *
     * @return ProfessionalExperience
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return string
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return ProfessionalExperience
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set startYear
     *
     * @param \DateTime $startYear
     *
     * @return ProfessionalExperience
     */
    public function setStartYear($startYear)
    {
        $this->startYear = $startYear;

        return $this;
    }

    /**
     * Get startYear
     *
     * @return \DateTime
     */
    public function getStartYear()
    {
        return $this->startYear;
    }

    /**
     * Set endYear
     *
     * @param \DateTime $endYear
     *
     * @return ProfessionalExperience
     */
    public function setEndYear($endYear)
    {
        $this->endYear = $endYear;

        return $this;
    }

    /**
     * Get endYear
     *
     * @return \DateTime
     */
    public function getEndYear()
    {
        return $this->endYear;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProfessionalExperience
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

//    public function __toString()
//    {
//        $sStartYear = $this->startYear->format('Y');
//        $sEndYear = $this->endYear->format('Y');
//        return "$this->title - $this->entreprise - $sStartYear - $sEndYear";
//    }

    #endregion
}

