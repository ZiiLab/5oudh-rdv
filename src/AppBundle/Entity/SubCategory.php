<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use AppBundle\Tools;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SubCategory
 */
class SubCategory
{
    use TimestampableTrait;
    #region **************************Attributes Region**************************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    #endregion

    #region **************************Mappings Region**************************************

    /**
     * @var Category
     */
    private $category;

    /**
     * @var ArrayCollection
     */
    private $rdvs;

    #endregion

    #region **************************Methods Region**************************************

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SubCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRdvs()
    {
        return $this->rdvs;
    }

    /**
     * @param ArrayCollection $rdvs
     */
    public function setRdvs($rdvs)
    {
        $this->rdvs = $rdvs;
        return $this;
    }


    public function setAttributes()
    {
        $this->slug = Tools::slugify($this->name);
    }

    public function __construct()
    {
        $this->rdvs = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    #endregion

}

