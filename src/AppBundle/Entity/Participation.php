<?PHP
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="participation")
 */
class Participation
{
	use TimestampableTrait;
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $session;

    /**
     * @ORM\Column(type="integer")
     */
    private $ticketkey;

		/**
		 * @ORM\Column(type="boolean")
		 */
		private $present = 0;

	    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

		/**
		 * @return User
		 */
		public function getUser()
		{
				return $this->user;
		}

		/**
		 * @param User $user
		 */
		public function setUser($user)
		{
				$this->user = $user;
				return $this;
		}


		/**
		 * @return Session
		 */
		public function getSession()
		{
				return $this->session;
		}

		/**
		 * @param Session $session
		 */
		public function setSession($session)
		{
				$this->session = $session;
				return $this;
		}

		/**
		 * @return Key
		 */
		public function getTicketkey()
		{
				return $this->key;
		}

		/**
		 * @param Key $key
		 */
		public function setTicketkey($ticketkey)
		{
				$this->ticketkey = $ticketkey;
				return $this;
		}

		/**
		 * @return Present
		 */
		public function getPresent()
		{
				return $this->present;
		}

		/**
		 * @param Present $present
		 */
		public function setPresent($present)
		{
				$this->present = $present;
				return $this;
		}

}
