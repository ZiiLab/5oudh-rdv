<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Place
 */
class Place
{
    use TimestampableTrait;
    #region *******************Attributes region***************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $address;
    #endregion

    #region *******************Mappings region***************************

    /**
     * @var City
     */
    private $city;

    /**
     * @var Delegation
     */
    private $delegation;
	
	 /**
     * @var Rdv
     */
    private $rdv;
    #endregion

    #region *******************Methods region***************************

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
	

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Place
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return Delegation
     */
    public function getDelegation()
    {
        return $this->delegation;
    }

    /**
     * @param Delegation $delegation
     */
    public function setDelegation($delegation)
    {
        $this->delegation = $delegation;
        return $this;
    }
	
	 public function __construct() {
        $this->rdvs = new ArrayCollection();
    }
    #endregion


}

