<?php

namespace AppBundle\Entity;
use AppBundle\Entity\Traits\TimestampableTrait;
use AppBundle\Tools;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * City
 */
class City
{
    use TimestampableTrait;
    #region **************************Attributes Region**************************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    #endregion

    #region **************************Mappings Region**************************************

    /**
     * @var ArrayCollection
     */
    private $delegations;

    #endregion

    #region **************************Methods Region**************************************

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return City
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return ArrayCollection
     */
    public function getDelegations()
    {
        return $this->delegations;
    }

    /**
     * @param Delegation $delegation
     */
    public function addDelegation($delegation)
    {
        $delegation->setCity($this);
        $this->delegations[] = $delegation;
        return $this;
    }

    /**
     * @param ArrayCollection $delegations
     */
    public function setDelegations($delegations)
    {
        $this->delegations = $delegations;
        return $this;
    }

    public function setAttributes()
    {
        $this->slug = Tools::slugify($this->name);
    }

    public function __construct()
    {
        $this->delegations = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }


    #endregion
}

