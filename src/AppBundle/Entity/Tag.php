<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use AppBundle\Tools;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tag
 */
class Tag
{
	
	
    use TimestampableTrait;
    #region *******************Attributes region***************************

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    #endregion

    #region *******************Mappings region***************************

      /**
     * @ORM\ManyToOne(targetEntity="Rdv", inversedBy="tags")
     * @ORM\JoinColumn(name="rdv_id", referencedColumnName="id")
     */
    private $rdv;

    #endregion

    #region *******************Methods region***************************

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Tag
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function setAttributes()
    {
        $this->slug = Tools::slugify($this->name);
    }

    /**
     * @return Rdv
     */
    public function getRdv()
    {
        return $this->rdv;
    }

    /**
     * @param Rdv $rdv
     */
    public function setRdv($rdv)
    {
        $this->rdv = $rdv;
        return $this;
    }

    /**
     * toString
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }


    #endregion
}

