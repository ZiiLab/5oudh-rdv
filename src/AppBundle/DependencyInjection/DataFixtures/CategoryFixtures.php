<?php

namespace AppBundle\DependencyInjection\DataFixtures;
use AppBundle\Entity\Category;
use AppBundle\Entity\SubCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */
class CategoryFixtures extends Fixture
{

    /**
     * Load SubCategory DataFixtures
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /*-------------------------------*/
        $oCategory =new Category();
        $oCategory->setName('Les RDV Technologie');
        $aSubCategoriesList = array(
            'Développement informatique',
            'Marketing Digital',
            'Bureautique',
            'Infographie Multimédia',
            'EnjeuxTechnologiques'
        );
        foreach ($aSubCategoriesList as $item) {
            $oSubCategory = new SubCategory();
            $oSubCategory->setName($item);
            $oCategory->addSubCategory($oSubCategory);
        }
        $this->addReference('firstSubcategory', $oSubCategory);
        $this->addReference('firstCategory', $oCategory);
        $manager->persist($oCategory);

        /*-------------------------------*/
        $oCategory =new Category();
        $oCategory->setName('Les RDV Business');
        $aSubCategoriesList = array(
            'Droit et législation',
            'Ressources Humaines',
            'Gestion Comptabilité Finance',
            'Achats et Supply Chain',
            'Qualité, sécurité,et développement durable',
            'Entrepreneuriat et stratégies d\'entreprises'
        );
        foreach ($aSubCategoriesList as $item) {
            $oSubCategory = new SubCategory();
            $oSubCategory->setName($item);
            $oCategory->addSubCategory($oSubCategory);
        }
        $this->addReference('secondSubcategory', $oSubCategory);
        $this->addReference('secondCategory', $oCategory);
        $manager->persist($oCategory);

        /*-------------------------------*/
        $oCategory =new Category();
        $oCategory->setName('Les RDV Coaching');
        $aSubCategoriesList = array(
            'Religion et spiritualité',
            'Etudes, emploi et carrière',
            'Santé, beauté et bien être',
            'Développement personnel',
        );
        foreach ($aSubCategoriesList as $item) {
            $oSubCategory = new SubCategory();
            $oSubCategory->setName($item);
            $oCategory->addSubCategory($oSubCategory);
        }
        $this->addReference('thirdSubcategory', $oSubCategory);
        $this->addReference('thirdCategory', $oCategory);
        $manager->persist($oCategory);

        /*-------------------------------*/
        $oCategory =new Category();
        $oCategory->setName('Les RDV Langues');
        $aSubCategoriesList = array(
            'Français',
            'Anglais',
            'Autres langues',
        );
        foreach ($aSubCategoriesList as $item) {
            $oSubCategory = new SubCategory();
            $oSubCategory->setName($item);
            $oCategory->addSubCategory($oSubCategory);
        }
        $this->addReference('fourthSubcategory', $oSubCategory);
        $this->addReference('fourthCategory', $oCategory);
        $manager->persist($oCategory);

        /*-------------------------------*/
        $oCategory =new Category();
        $oCategory->setName('Les RDV Créativité');
        $aSubCategoriesList = array(
            'Cuisine et arts culinaires',
            'Couture, bijoux et créations artisanales',
            'Bricolage et travaux divers',
            'Ecriture, dessin et peinture'
        );
        foreach ($aSubCategoriesList as $item) {
            $oSubCategory = new SubCategory();
            $oSubCategory->setName($item);
            $oCategory->addSubCategory($oSubCategory);
        }
        $this->addReference('finthSubcategory', $oSubCategory);
        $this->addReference('finthCategory', $oCategory);
        $manager->persist($oCategory);

        /*-------------------------------*/
        $oCategory =new Category();
        $oCategory->setName('Les RDV Loisirs');
        $aSubCategoriesList = array(
            'Musique',
            'Théatre',
            'Photographie, cinéma, et arts divers',
            'Activités sportives et sorties'
        );
        foreach ($aSubCategoriesList as $item) {
            $oSubCategory = new SubCategory();
            $oSubCategory->setName($item);
            $oCategory->addSubCategory($oSubCategory);
        }
        $this->addReference('sixSubcategory', $oSubCategory);
        $this->addReference('sixCategory', $oCategory);
        $manager->persist($oCategory);

        $manager->flush();
    }
}
