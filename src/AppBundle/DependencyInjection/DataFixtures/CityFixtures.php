<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\DependencyInjection\DataFixtures;


use AppBundle\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CityFixtures extends Fixture
{

    /**
     * Load City Fixtures
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Ariana');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Béja');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Ben Arous');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Bizerte');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Gabes');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Gafsa');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Jendouba');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Kairouan');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Kasserine');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Kebili');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('La Manouba');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Le Kef');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Mahdia');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Médenine');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Monastir');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Nabeul');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Sfax');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Sidi Bouzid');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Siliana');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Sousse');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Tataouine');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Tozeur');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Tunis');
        $manager->persist($oCity);
        /*-------------------------------*/
        $oCity = new City();
        $oCity->setName('Zaghouane');
        $manager->persist($oCity);

        $manager->flush();
    }
}
