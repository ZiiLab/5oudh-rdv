<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\DependencyInjection\DataFixtures;


use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AdminFixtures extends Fixture
{
    /**
     * Load admin datafixtures
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
      /*************** Admin User *******************/
        $oUser = new User();

        $oUser->setUsername('admin')
              ->setEmail('admin@admin.com')
              ->setEnabled(true)
              ->setCin(0)
              ->setPlainPassword('admin5oudhRdv')
              ->setAdmin(true)
              ->setRoles(['ROLE_ADMIN']);

        $manager->persist($oUser);

  /***************  User 1 *******************/
        $oUser = new User();

        $oUser->setUsername('med')
              ->setEmail('med@med.com')
              ->setEnabled(true)
              ->setCin(1)
              ->setPlainPassword('user')
              ->setAdmin(false)
              ->setRoles(['ROLE_USER']);
        $this->addReference('firstUser', $oUser);
        $manager->persist($oUser);


/***************  User 2 *******************/
        $oUser = new User();
        $oUser->setUsername('zac')
              ->setEmail('zac@med.com')
              ->setEnabled(true)
              ->setCin(2)
              ->setPlainPassword('user')
              ->setAdmin(false)
              ->setRoles(['ROLE_USER']);
        $this->addReference('secondUser', $oUser);
        $manager->persist($oUser);
/***************  User 3 *******************/
        $oUser = new User();
        $oUser->setUsername('monta')
              ->setEmail('monta@med.com')
              ->setEnabled(true)
              ->setCin(3)
              ->setPlainPassword('user')
              ->setAdmin(false)
              ->setRoles(['ROLE_USER']);
        $this->addReference('thirdUser', $oUser);
        $manager->persist($oUser);


        $manager->flush();
    }
}
