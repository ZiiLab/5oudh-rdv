<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\DependencyInjection\DataFixtures;


use AppBundle\Entity\Rdv;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RdvsFixtures extends Fixture
{
    /**
     * Load admin datafixtures
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
    /***************** Rdv 1*********************/
        $rdv = new Rdv();
        $rdv->setTitle("Rdv 1");
        $rdv->setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
        $rdv->setCategory($this->getReference('firstCategory'));
        $rdv->setSubCategory($this->getReference('firstSubcategory'));
        $rdv->setAuthor($this->getReference('firstUser'));
        $rdv->setNiveau("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
        $rdv->setPublished(false);
        $manager->persist($rdv);

    /***************** Rdv 2*********************/
        $rdv = new Rdv();
        $rdv->setTitle("Rdv 2");
        $rdv->setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
        $rdv->setCategory($this->getReference('secondCategory'));
        $rdv->setSubCategory($this->getReference('secondSubcategory'));
        $rdv->setAuthor($this->getReference('secondUser'));
        $rdv->setNiveau("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
        $rdv->setPublished(true);
        $manager->persist($rdv);

    /***************** Rdv 3*********************/
        $rdv = new Rdv();
        $rdv->setTitle("Rdv 3");
        $rdv->setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
        $rdv->setCategory($this->getReference('thirdCategory'));
        $rdv->setSubCategory($this->getReference('thirdSubcategory'));
        $rdv->setAuthor($this->getReference('thirdUser'));
        $rdv->setNiveau("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
        $rdv->setPublished(true);
        $manager->persist($rdv);

    /***************** Rdv 4*********************/
        $rdv = new Rdv();
        $rdv->setTitle("Rdv 4");
        $rdv->setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
        $rdv->setCategory($this->getReference('fourthCategory'));
        $rdv->setSubCategory($this->getReference('fourthSubcategory'));
        $rdv->setAuthor($this->getReference('firstUser'));
        $rdv->setNiveau("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
        $rdv->setPublished(false);
        $manager->persist($rdv);

  /***************** Rdv 5*********************/
      $rdv = new Rdv();
      $rdv->setTitle("Rdv 5");
      $rdv->setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
      $rdv->setCategory($this->getReference('finthCategory'));
      $rdv->setSubCategory($this->getReference('finthSubcategory'));
      $rdv->setAuthor($this->getReference('secondUser'));
      $rdv->setNiveau("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.");
      $rdv->setPublished(false);
      $manager->persist($rdv);
          $manager->flush();

    }
}
