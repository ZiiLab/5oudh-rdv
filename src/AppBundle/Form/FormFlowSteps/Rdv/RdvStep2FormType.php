<?php

/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */
namespace AppBundle\Form\FormFlowSteps\Rdv;

use AppBundle\Form\SessionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class RdvStep2FormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        $poBuilder
            ->add('sessions', CollectionType::class, [
                'entry_type' => SessionType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'by_reference' => false,
                'label' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_rdv_step2';
    }
}