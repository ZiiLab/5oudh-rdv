<?php

/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */
namespace AppBundle\Form\FormFlowSteps\Rdv;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Vich\UploaderBundle\Form\Type\VichFileType;

class RdvStep3FormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        $poBuilder
            ->add('imageFile', VichFileType::class, [
                'required'      => true,
                'allow_delete'  => false,
                'download_link' => false,
                'attr'=>['class'=>'form-control-file'],
            ])
            ->add('terms1', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'constraints' => new IsTrue(
                    ['message' => "Vous devez accepter les conditions d'utilisation."]
                )
            ])
            ->add('terms2', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'constraints' => new IsTrue(
                    ['message' => "Vous devez accepter les conditions d'utilisation."]
                )
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_rdv_step3';
    }
}