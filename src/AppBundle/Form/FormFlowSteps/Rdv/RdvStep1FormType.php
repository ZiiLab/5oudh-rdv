<?php

/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */
namespace AppBundle\Form\FormFlowSteps\Rdv;

use AppBundle\Form\TagType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RdvStep1FormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        $poBuilder
            ->add('title', TextType::class)
            ->add('category', EntityType::class, [
                'choice_label' => 'name',
                'class' => 'AppBundle\Entity\Category',
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('subCategory', EntityType::class, [
                'choice_label' => 'name',
                'class' => 'AppBundle\Entity\SubCategory',
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('tags', CollectionType::class, array(
                'entry_type' => TagType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'by_reference' => false,
            ))
            ->add('description', TextType::class)
            ->add('niveau', ChoiceType::class, [
                'choices' => [
                    'Débutant' => 'Débutant',
                    'Intermédiaire' => 'Intermédiaire',
                    'Avancé' => 'Avancé',
                ],
                'expanded' => false,
                'multiple' => false,
            ])
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_rdv_step1';
    }
}