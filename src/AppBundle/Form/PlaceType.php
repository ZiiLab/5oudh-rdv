<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        $poBuilder
            ->add('address', TextType::class, [
                'label' => "Adresse",
            ])
            ->add('city', EntityType::class, array(
                'label' => 'Gouvernorat:',
                'class' => 'AppBundle\Entity\City',
                'choice_label' => 'name',
            ))
            ->add('delegation', EntityType::class, array(
                'label' => 'Délegation:',
                'class' => 'AppBundle\Entity\Delegation',
                'choice_label' => 'name',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $poResolver)
    {
        $poResolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Place',
            'csrf_protection' => true,
        ));
    }
}