<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        $poBuilder->add('school', TextType::class, array(
                        'label' => "Ecole",
                    )
        )
                  ->add('diploma', TextType::class, array(
                      'label' => "Diplome",
                  ))
                  ->add('studyField', TextType::class, array(
                      'label' => "Domaine d'étude",
                  ))
                  ->add('startYear', DateType::class, array(
                      'label' => "De l'année",
                      'widget' => 'single_text',
                      'format' => 'yyyy',
                  ))
                  ->add('endYear', DateType::class, array(
                      'label' => "A l'année",
                      'widget' => 'single_text',
                      'format' => 'yyyy',
                  ))
                  ->add('description', TextType::class, array(
                      'label' => "Description",
                  ));
    }

    public function configureOptions(OptionsResolver $poResolver)
    {
        $poResolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Education',
            'csrf_protection' => true,
        ));
    }
}