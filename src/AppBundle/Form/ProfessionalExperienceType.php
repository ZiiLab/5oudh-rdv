<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfessionalExperienceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        $poBuilder->add('title', TextType::class, array(
            'label' => "Intitulé du poste",
        ))
            ->add('entreprise', TextType::class, array(
                'label' => "Entreprise",
            ))
            ->add('place', TextType::class, array(
                'label' => "Lieu",
            ))
            ->add('startYear', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy',
                'label' => "De l'année",
            ))
            ->add('endYear', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy',
                'label' => "A l'année",
            ))
            ->add('description', TextType::class, array(
                'label' => "Description",
            ));
    }

    public function configureOptions(OptionsResolver $poResolver)
    {
        $poResolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ProfessionalExperience',
            'csrf_protection' => true,
        ));
    }
}