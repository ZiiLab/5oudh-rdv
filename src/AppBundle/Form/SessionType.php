<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        $poBuilder
            ->add('dateSession', DateType::class, [
                'label' => "Jour",
                'widget' => 'single_text',
            ])
            ->add('startTime', TimeType::class, [
                'label' => "Heure début",
                'input'  => 'datetime',
                'widget' => 'single_text',
                'html5' => true,
                'with_seconds' => false,
            ])
            ->add('endTime', TimeType::class, [
                'label' => "Heure fin",
                'input'  => 'datetime',
                'widget' => 'single_text',
                'html5' => true,
                'with_seconds' => false,
            ])
            ->add('price', NumberType::class, [
                'label' => "Prix <sup>DTN</sup>",
            ])
            ->add('nbrParticipants', NumberType::class, [
                'label' => "Nb.Participants",
            ])
            ->add('place', PlaceType::class, [
                'label' => "Lieu",
            ])
            ;
    }

    public function configureOptions(OptionsResolver $poResolver)
    {
        $poResolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Session',
            'csrf_protection' => true,
        ));
    }
}