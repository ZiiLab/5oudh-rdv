<?php

namespace AppBundle\Form;
use function Sodium\add;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */
class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        $poBuilder
            ->add('cin', TextType::class)
            ->add('terms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => new IsTrue([
                    'message' =>  "Vous devez accepter les conditions d'utilisation.",
                ]),
            ]);
    }

    public function configureOptions(OptionsResolver $poResolver)
    {
        $poResolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'csrf_protection' => true,
        ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}