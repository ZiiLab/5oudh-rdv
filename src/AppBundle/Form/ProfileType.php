<?php

namespace AppBundle\Form;

use AppBundle\Entity\City;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */
class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $poBuilder, array $paOptions)
    {
        //Form modifier clause to change delegation field dynamically
        $formModifier = function (FormInterface $poForm, City $poCity = null) {
            $aDelegations = null === $poCity ? array() : $poCity->getDelegations();

            $poForm->remove('delegation');
            $poForm->add('delegation', EntityType::class, array(
                'class' => 'AppBundle\Entity\Delegation',
                'choice_label' => 'name',
                'choices' => $aDelegations,
            ));
        };

        $poBuilder
                  ->remove('current_password')
                  ->add('firstName', TextType::class, array(
                      'label' => 'Prénom:',
                  ))
                  ->add('lastName', TextType::class, array(
                      'label' => 'Nom:',
                  ))
                  ->add('username', TextType::class, array(
                      'label' => 'Pseudo:',
                  ))
                  ->add('email', EmailType::class, array(
                      'label' => 'Email:',
                  ))
                  ->add('plainPassword', RepeatedType::class, array(
                      'type' => PasswordType::class,
                      'label' => 'Mot de passe:',
                      'first_options'  => array('label' => 'Mot de passe:'),
                      'second_options' => array('label' => 'Confirmer mot de passe:')
                  ))
                  ->add('gender', ChoiceType::class, array(
                      'label' => 'Genre:',
                      'choices' => array(
                          'Homme' => 'Homme',
                          'Femme' => 'Femme'
                      ),
                      'expanded' => true,
                      'multiple' => false,
                  ))
                  ->add('birthDate', BirthdayType::class, array(
                      'label' => 'Date de naissance:',
                      'widget' => 'single_text',
                  ))
                  ->add('cin', NumberType::class, array(
                      'label' => 'Numéro carte identité:',
                  ))
                  ->add('phone', TelType::class, array(
                      'label' => 'Num Tel:',
                  ))
                  ->add('address', TextType::class, array(
                      'label' => 'Adresse:',
                  ))
                  ->add('city', EntityType::class, array(
                      'label' => 'Gouvernorat:',
                      'class' => 'AppBundle\Entity\City',
                      'choice_label' => 'name',
                  ))
                  ->add('delegation', EntityType::class, array(
                      'label' => 'Délegation:',
                      'class' => 'AppBundle\Entity\Delegation',
                      'choice_label' => 'name',
                  ))
                  ->add('zip', NumberType::class, array(
                      'label' => 'Code postale:'
                  ))
                  ->add('biography', TextareaType::class, array(
                      'label' => 'A propos de vous:'
                  ))
                  ->add('proSituation', ChoiceType::class, array(
                      'label' => 'Votre situation actuelle:',
                      'choices' => array(
                          "Fonctionnaire" => "Fonctionnaire",
                          "Etudiant(e)" => "Etudiant(e)",
                          'Salarié(e)' => 'Salarié(e)',
                          "Indépendant-Freelancer" => "Indépendant-Freelancer",
                          "Enseignant(e)" => "Enseignant(e)",
                          "Chef d'entreprise" => "Chef d'entreprise",
                          "Retraité(e)" => "Retraité(e)",
                          "A la recherche d'emploi" => "A la recherche d'emploi",


                      ),
                      'expanded' => false,
                      'multiple' => false
                  ))
                  ->add('education', ChoiceType::class, array(
                      'label' => 'Votre niveau académique:',
                      'choices' => array(
                          'Secondaire' => 'Secondaire',
                          'Universitaire' => 'Universitaire',
                      ),
                      'expanded' => false,
                      'multiple' => false
                  ))->add('diplomas', CollectionType::class, array(
                        'label' => 'Diplomes:',
                        'entry_type' => EducationType::class,
                        'entry_options' => array('label' => false),
                        'allow_add' => true,
                        'by_reference' => false,
                  ))->add('professionalExperiences', CollectionType::class, array(
                      'label' => 'Expériences professionelles:',
                      'entry_type' => ProfessionalExperienceType::class,
                      'entry_options' => array('label' => false),
                      'allow_add' => true,
                      'by_reference' => false,
                  ))->add('interests', EntityType::class, array(
                      'label' => false,
                      'class' => 'AppBundle\Entity\Interest',
                      'choice_label' => 'name',
                      'expanded' => true,
                      'multiple' => true,
                  ))
                ->add('imageFile', VichFileType::class, [
                    'required'      => true,
                    'allow_delete'  => false,
                    'download_link' => false,
                    'attr'=>['class'=>'file'],
                ]);;

        //Form events to show delegations for only selected city

        $poBuilder->get('city')->addEventListener(FormEvents::POST_SET_DATA,
            function (FormEvent $poEvent) use($formModifier) {
                $oCity = $poEvent->getForm()->getData();
                if ($oCity)
                    $formModifier($poEvent->getForm()->getParent(), $oCity);
            }
        );

        $poBuilder->get('city')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {

                $oCity = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $oCity);
            }
        );
    }

    public function configureOptions(OptionsResolver $poResolver)
    {
        $poResolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'csrf_protection' => false,
            'required' => false,
        ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }
}