<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\Form;

use Craue\FormFlowBundle\Form\FormFlow;

class RdvFormFlow extends FormFlow
{
    protected $allowDynamicStepNavigation = true;

    protected function loadStepsConfig() {
        return array(
            array(
                'label' => 'Description RDV',
                'form_type' => 'AppBundle\Form\FormFlowSteps\Rdv\RdvStep1FormType',
            ),
            array(
                'label' => 'Planification RDV',
                'form_type' => 'AppBundle\Form\FormFlowSteps\Rdv\RdvStep2FormType',
            ),
            array(
                'label' => 'Publication',
                'form_type' => 'AppBundle\Form\FormFlowSteps\Rdv\RdvStep3FormType',
            ),
        );
    }
}