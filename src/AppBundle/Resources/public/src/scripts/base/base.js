$(document).ready(function () {
    $("ul[id*='item-rdv-']").hide();
    $(document).on("click", ".rdv-item-list", function () {
        $(".rdv-item-list").removeClass("active-rdv");
        $(this).addClass("active-rdv");
        $("ul[id*='item-rdv-']").hide();
        var rdvId = $(this).attr("id");
        $("#" + "item-" + rdvId).toggle();
    });

    //coté modal email
    $("#rdvInputEmail").focusin(function () {
        $(this).removeClass("valid-put");
        $(this).removeClass("invalid-put");
        $("#emailHelp").html("");
        $(this).focusout(function () {
            var email = $(this).val();
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!email) {
                $("#emailHelp").html("<span style='color: red'>&nbspChamps obligatoire</span>");
                $(this).addClass("invalid-put");
            } else {
                if (regex.test(email)) {
                    $("#emailHelp").html("<i class=\"fas fa-check\" style='color: green'>&nbspformat vérifié</i>");
                    $(this).addClass("valid-put");
                    //$(".modalbtn").attr("data-dismiss", "modal");
                } else {
                    $("#emailHelp").html("<span style='color: red'>&nbspformat incorrect</span>");
                    $(this).addClass("invalid-put");
                }
            }

        });
    });
    //coté carte d'identité
    $("#rdvInputcartid").focusin(function () {
        $(this).removeClass("valid-put");
        $(this).removeClass("invalid-put");
        $("#numcartidHelp").html("");
        $(this).focusout(function () {
            var cartid = $(this).val();
            if (!cartid) {
                $("#numcartidHelp").html("<span style='color: red'>&nbspChamps obligatoire</span>");
                $(this).addClass("invalid-put");
            } else {
                if (($.isNumeric(cartid)) && (cartid.length === 8)) {
                    $("#numcartidHelp").html("<i class=\"fas fa-check\" style='color: green'>&nbspformat vérifié</i>");
                    $(this).addClass("valid-put");
                    //$(".modalbtn").attr("data-dismiss", "modal");
                } else {
                    $("#numcartidHelp").html("<span style='color: red'>&nbspformat incorrect</span>");
                    $(this).addClass("invalid-put");
                }
            }

        });
    });

    //coté mot de passe1
    $("#rdvInputPassword").focusin(function () {
        $(this).removeClass("valid-put");
        $(this).removeClass("invalid-put");
        $("#passwrdHelp").html("");
        $(this).focusout(function () {
            var password = $(this).val();
            var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
            if (!password) {
                $("#passwrdHelp").html("<span style='color: red'>&nbspChamps obligatoire</span>");
                $(this).addClass("invalid-put");
            } else {
                if (regex.test(password)) {
                    $("#passwrdHelp").html("<i class=\"fas fa-check\" style='color: green'>&nbspformat vérifié</i>");
                    $(this).addClass("valid-put");
                    //$(".modalbtn").attr("data-dismiss", "modal");
                } else {
                    $("#passwrdHelp").html("<span style='color: red'>&nbspformat incorrect</span>");
                    $(this).addClass("invalid-put");
                }

            }
        });
    });

    //coté confirmation mot de passe1
    $("#rdvInputPasswordconf").focusin(function () {
        $(this).removeClass("valid-put");
        $(this).removeClass("invalid-put");
        $("#passwrd1Help").html("");
        $(this).focusout(function () {
            var password = $("#rdvInputPassword").val();
            var confpassword = $(this).val();
            if (password === confpassword) {
                $("#passwrd1Help").html("<i class=\"fas fa-check\" style='color: green'>&nbspformat vérifié</i>");
                $(this).addClass("valid-put");
                //$(".modalbtn").attr("data-dismiss", "modal");
            } else {

                $("#passwrd1Help").html("<span style='color: red'>&nbspformat incorrect</span>");
                $(this).addClass("invalid-put");

            }

        });
    });
    $("#info-cartid").hover(function () {
        $(".arr-cartinf").toggle("slow");
    })
});