<?php

namespace AppBundle\EventListener;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */
class ProfileListener implements EventSubscriberInterface
{
    private $oContainer;

    public function __construct(ContainerInterface $poContainer)
    {
        $this->oContainer = $poContainer;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::PROFILE_EDIT_SUCCESS => 'onProfileEditSuccess',
        );
    }

    public function onProfileEditSuccess(FormEvent $poEvent)
    {
        $sProfileEditUrl = $this->oContainer->get('router')->generate('fos_user_profile_edit');
        $poEvent->setResponse(new RedirectResponse($sProfileEditUrl));
    }
}