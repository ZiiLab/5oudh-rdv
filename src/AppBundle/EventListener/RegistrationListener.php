<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Psr\Container\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RegistrationListener implements EventSubscriberInterface
{
    private $oContainer;

    public function __construct(ContainerInterface $poContainer)
    {
        $this->oContainer = $poContainer;
    }
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
        );
    }

    public function onRegistrationConfirm(GetResponseUserEvent $poEvent)
    {
        $sProfileEditUrl = $this->oContainer->get('router')->generate('fos_user_profile_edit');
        $poEvent->setResponse(new RedirectResponse($sProfileEditUrl));

    }
}