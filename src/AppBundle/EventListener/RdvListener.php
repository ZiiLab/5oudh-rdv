<?php
/**
 * Created by Safouane Fakhfakh.
 * Email: Safouane.Fakhfakh@mail.com
 */

namespace AppBundle\EventListener;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Psr\Container\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RdvListener
{
    private $oContainer;

    public function __construct(ContainerInterface $poContainer)
    {
        $this->oContainer = $poContainer;
    }

    public function preUpdate(PreUpdateEventArgs $poEvent)
    {
        if ($poEvent->hasChangedField('published')) {

            $oMailingService = $this->oContainer->get('app.service.mailing');
            $oRouter = $this->oContainer->get('router');

            $oEntity = $poEvent->getEntity();
            $oEmail = $oMailingService->makeEmail(
                'Votre annonce a été publié',
                $oEntity->getAuthor()->getEmail(),
                [
                    'firstname' => $oEntity->getAuthor()->getFirstName(),
                    'ad_url' => $oRouter->generate('ad_details', ['id' => $oEntity->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
                    'ad_title' => $oEntity->getTitle()
                ],
                'AppBundle:emails:ad-published.html.twig'

            );
            $oMailingService->sendEmail($oEmail);
        }
    }
}